﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformEndMarker : MonoBehaviour {

    [SerializeField] int pointValue = 1;

    private void OnTriggerExit2D(Collider2D collision) {
        Player player = collision.GetComponent<Player>();
        if (player) {
            player.AddPoints(pointValue);
        }
    }
}
