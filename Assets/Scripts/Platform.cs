﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {

    public Transform endMarkerTransform;

    private Transform playerTransform;

	// Use this for initialization
	void Start () {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
	}

    private void Update() {
        CheckIfOffScreen();
    }

    // Checks if the platform is off screen and destroys self if true
    void CheckIfOffScreen() {
        if (transform.position.x < playerTransform.position.x - 20f) {
            Destroy(this.gameObject);
            FindObjectOfType<PlatformSpawner>().SpawnPlatform();
        }
    }
}
