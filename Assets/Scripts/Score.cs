﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    private int currentScore;
    private Text scoreText;

	// Use this for initialization
	void Start () {
        currentScore = 0;
        scoreText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        UpdateText();
    }

    // Adds points to the current score
    public void AddPoints(int points) {
        currentScore += points;
    }

    // Returns the current score
    public int GetScore() {
        return currentScore;
    }
    
    // Resets the score back to its starting value
    public void ResetScore() {
        currentScore = 0;
    }

    // Updates the score text
    void UpdateText() {
        scoreText.text = currentScore.ToString();
    }
}
