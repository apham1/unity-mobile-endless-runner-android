﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class GoogleMobileAdsScript : MonoBehaviour {
    private BannerView bannerView;
    private InterstitialAd interstintialView;

	// Use this for initialization
	void Start () {
        InitializeMobileAds();
        RequestBanner();
        RequestInterstitial();
    }

    private void RequestBanner() {
        #if UNITY_ANDROID
            string adUnitId = "ca-app-pub-1850083321624431/3010070989";
        #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
        #else
            string adUnitId = "unexpected_platform";
        #endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    private void RequestInterstitial() {
        #if UNITY_ANDROID
            string adUnitId = "ca-app-pub-1850083321624431/3369163635";
        #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/4411468910";
        #else
            string adUnitId = "unexpected_platform";
        #endif

        // Initialize an InterstitialAd.
        interstintialView = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstintialView.LoadAd(request);
    }

    private void InitializeMobileAds() {
        #if UNITY_ANDROID
            string appId = "ca-app-pub-1850083321624431~9770920455";
        #elif UNITY_IPHONE
            string appId = "ca-app-pub-3940256099942544~1458002511";
        #else
            string appId = "unexpected_platform";
        #endif
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
    }

    public void ShowInterstitial() {
        if (interstintialView.IsLoaded()) {
            interstintialView.Show();
        }
    }
}
