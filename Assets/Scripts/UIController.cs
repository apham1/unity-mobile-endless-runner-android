﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

    public GameObject gameOverScreen;
    public GameObject score;

    // Sets the activity state of the game over text
    public void SetGameOverScreen(bool flag) {
        gameOverScreen.SetActive(flag);
    }

    // Sets the activity state of the score text
    public void SetScoreText(bool flag) {
        score.SetActive(flag);
    }

    // Returns the game over text game object
    public GameObject GetGameOverScreen() {
        return gameOverScreen;
    }
}
