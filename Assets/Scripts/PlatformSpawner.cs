﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour {

    enum Platforms {
        DEFAULT,
        SMALL,
        SPIKEGAP,
        STAIR,
        SPIKEDMIDDLE,
        SPIKEDTUNNEL,
        SPIKEDROOF,
        DOUBLESPIKED,
        END
    }

    [SerializeField] float platformSpawnDistanceApart = 3f;
    [SerializeField] float platformSpawnHeightDifferenceMin = -1.5f;
    [SerializeField] float platformSpawnHeightDifferenceMax = 0.5f;

    public List<GameObject> platformPrefabs;

    public Transform deathTransform;

    public Score score;

    private Transform prevPlatformTransform;
    private const int MaxPlatforms = 10;

	// Use this for initialization
	void Start () {
        Reset();
	}

    // Spawns a platform in a reachable position ahead of the player
    public void SpawnPlatform() {
        Vector3 spawnPosition = new Vector3(prevPlatformTransform.position.x + platformSpawnDistanceApart, prevPlatformTransform.position.y + Random.Range(platformSpawnHeightDifferenceMin, platformSpawnHeightDifferenceMax));
        if (spawnPosition.y <= deathTransform.position.y + 5)
            spawnPosition.y = deathTransform.position.y + 5;

        int choiceLimit;
        if (score.GetScore() / 10 + 2 < (int)Platforms.END) {
            choiceLimit = score.GetScore() / 10 + 2;
        } else {
            choiceLimit = (int)Platforms.END;
        }
        GameObject platform = Instantiate(ChoosePlatform(Random.Range(0, choiceLimit)), spawnPosition, new Quaternion());
        prevPlatformTransform = platform.GetComponent<Platform>().endMarkerTransform;
    }

    // Resets the platforms back to the starting state
    public void Reset() {
        DestroyPlatforms();

        GameObject platform = Instantiate(ChoosePlatform((int)Platforms.DEFAULT), new Vector3(-2f, -1f), new Quaternion());
        prevPlatformTransform = platform.GetComponent<Platform>().endMarkerTransform;
        for (int i = 0; i < 9; i++) {
            SpawnPlatform();
        }
    }

    // Returns a platform game object
    GameObject ChoosePlatform(int selection) {
        return platformPrefabs[selection];
    }

    // Destroys all platforms in the scene
    void DestroyPlatforms() {
        Platform[] allPlatforms = FindObjectsOfType<Platform>();

        foreach (Platform platform in allPlatforms) {
            Destroy(platform.gameObject);
        }
    }
}
