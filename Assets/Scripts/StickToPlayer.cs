﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickToPlayer : MonoBehaviour {

    [SerializeField] float xOffset = 0.0f;
    [SerializeField] float yOffset = 0.0f;

    [SerializeField] bool stickToX = true;
    [SerializeField] bool stickToY = true;

    public Transform playerTransform;

	// Update is called once per frame
	void Update () {
        if (stickToX) {
            MoveToPlayerX();
        }

        if (stickToY) {
            MoveToPlayerY();
        }
	}

    // Moves Object to Player X Position
    void MoveToPlayerX() {
        transform.position = new Vector3(playerTransform.position.x + xOffset, transform.position.y);
    }

    // Moves Object to Player Y Position
    void MoveToPlayerY() {
        transform.position = new Vector3(transform.position.x, playerTransform.position.y + yOffset);
    }
}
