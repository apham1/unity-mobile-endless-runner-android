﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField] float cameraDistance = 10f;

    public Transform playerTransform;
    public Transform deathTransform;
	
	// Update is called once per frame
	void Update () {
        MoveCamera();
	}

    // Moves the camera to the player, but not out of the level bounds
    void MoveCamera() {
        if (playerTransform.position.y - Camera.main.orthographicSize >= deathTransform.position.y) {
            transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, playerTransform.position.z - cameraDistance);
        } else {
            transform.position = new Vector3(playerTransform.position.x, deathTransform.position.y + Camera.main.orthographicSize, playerTransform.position.z - cameraDistance);
        }
    }
}
