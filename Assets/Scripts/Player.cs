﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    [SerializeField] float playerSpeed = 5f;
    [SerializeField] float jumpSpeed = 5f;
    [SerializeField] float jumpRotationTime = 1f;
    [SerializeField] float jumpRotationAmount = 90f;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask groundLayer;

    public GoogleMobileAdsScript adController;

    public Score score;
    public DeathPitController pitController;

    private Rigidbody2D rigidBody;
    private AudioSource jumpSound;
    private bool isGrounded;
    private bool isAlive;

	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody2D>();
        jumpSound = GetComponent<AudioSource>();
        isAlive = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (isAlive) {
            MovePlayer();
            CheckGrounded();
            Jump();
        }
	}

    // Moves the player towards the right. 
    void MovePlayer() {
        rigidBody.velocity = new Vector2(playerSpeed, rigidBody.velocity.y);
    }

    // Makes the player jump.
    void Jump() {
        if (Input.GetMouseButtonDown(0) && isGrounded) {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, jumpSpeed);
            jumpSound.Play();
            StartCoroutine(RotatePlayer(Vector3.forward * -jumpRotationAmount, jumpRotationTime));
        }
    }

    // Rotates the player 
    IEnumerator RotatePlayer(Vector3 angle, float time) {
        StopRotation();

        Quaternion startAngle = transform.rotation;
        Quaternion endAngle = Quaternion.Euler(transform.eulerAngles + angle);
        for (float i = 0f; i < 1f; i += Time.deltaTime / time) {
            if (isAlive) {
                transform.rotation = Quaternion.Slerp(startAngle, endAngle, i);
                yield return null;
            }
        }
    }

    void StopRotation() {
        rigidBody.angularVelocity = 0f;
    }

    // Checks if the player is grounded.
    void CheckGrounded() {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
    }

    // Kills the player
    public void Die() {
        isAlive = false;
        rigidBody.velocity = new Vector2(0f, rigidBody.velocity.y);
        rigidBody.constraints = RigidbodyConstraints2D.FreezeAll;

        GameOver();
    }

    private void GameOver() {
        LoseScreen loseScreen = FindObjectOfType<LoseScreen>();
        loseScreen.UpdateScores();
        loseScreen.ToggleScreen();
    }

    // Returns true if the player is alive, false if not
    public bool IsAlive() {
        return isAlive;
    }

    // Restarts the game
    public void RestartGame() {
        LoseScreen loseScreen = FindObjectOfType<LoseScreen>();
        loseScreen.ToggleScreen();
    
        ResetPlayer();
        pitController.ResetPosition();
    
        score.ResetScore();
    
        FindObjectOfType<PlatformSpawner>().Reset();
    }

    // Resets the player
    void ResetPlayer() {
        isAlive = true;

        transform.position = new Vector3(0f, 0.5f);
        rigidBody.constraints = RigidbodyConstraints2D.None;
        rigidBody.rotation = 0f;
        rigidBody.velocity = new Vector3(0f, 0f);
        rigidBody.angularVelocity = 0f;
    }

    // Adds points to the score
    public void AddPoints(int points) {
        score.AddPoints(1);
    }
}
