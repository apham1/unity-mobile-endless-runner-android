﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverMenu : MonoBehaviour {

    public Player player;

    public void StartGame() {
        player.RestartGame();
    }

    public void ShowLeaderboard() {

    }
}
