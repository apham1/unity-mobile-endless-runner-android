﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathPitController : MonoBehaviour {

    public Transform playerTransform;
	
	// Update is called once per frame
	void Update () {
        AdjustDistanceFromPlayer();
	}

    void AdjustDistanceFromPlayer() {
        if (transform.position.y < playerTransform.position.y - 20f) {
            transform.position = new Vector3(0f, playerTransform.position.y - 20f);
        }
    }

    public void ResetPosition() {
        transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y - 12.5f);
    }
}
