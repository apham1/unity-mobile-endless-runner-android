﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoseScreen : MonoBehaviour {

    public Text finalScoreText;
    public Text topScoreText;
    public Score scoreComponent;

    private int finalScore;
    private int highScore;

	// Use this for initialization
	void Start () {
        highScore = PlayerPrefs.GetInt("highscore");
	}

    // Updates the scoreboard
    public void UpdateScores() {
        finalScore = scoreComponent.GetScore();
        if (finalScore > highScore) {
            highScore = finalScore;
            PlayerPrefs.SetInt("highscore", highScore);
        }

        UpdateScoreText();
    }

    // Updates the lose screen score text
    void UpdateScoreText() {
        finalScoreText.text = "Your Score: " + finalScore.ToString();
        topScoreText.text = "High Score: " + highScore.ToString();
    }
    
    // Toggles the lose screen
    public void ToggleScreen() {
        UIController controller = GetComponent<UIController>();

        if (controller) {
            if (controller.GetGameOverScreen().activeSelf) { 
                controller.SetGameOverScreen(false);
                controller.SetScoreText(true);
            } else {
                controller.SetGameOverScreen(true);
                controller.SetScoreText(false);
            }
        }
    }
}
